/*
SQLyog Ultimate v13.1.1 (64 bit)
MySQL - 8.0.13 : Database - mydb
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`mydb` /*!40100 DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci */;

USE `mydb`;

/*Table structure for table `course` */

DROP TABLE IF EXISTS `course`;

CREATE TABLE `course` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '课程名称',
  `remark` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

/*Data for the table `course` */

insert  into `course`(`id`,`name`,`remark`) values 
(1,'C语言',NULL),
(2,'Java',NULL),
(3,'Python',NULL);

/*Table structure for table `e_p` */

DROP TABLE IF EXISTS `e_p`;

CREATE TABLE `e_p` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `exam_id` int(11) DEFAULT NULL COMMENT '考试id',
  `paper_id` int(11) DEFAULT NULL COMMENT '试卷id',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

/*Data for the table `e_p` */

insert  into `e_p`(`id`,`exam_id`,`paper_id`) values 
(2,2,2),
(3,3,3),
(4,1,1);

/*Table structure for table `exam` */

DROP TABLE IF EXISTS `exam`;

CREATE TABLE `exam` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '考试名称',
  `room` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT '考场',
  `time` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT '考试时间',
  `invigilator` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '监考老师',
  `state` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT '未开始' COMMENT '考试状态',
  `course_id` int(11) DEFAULT NULL COMMENT '课程id',
  `duration` int(11) DEFAULT '150' COMMENT '持续时间（分）',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

/*Data for the table `exam` */

insert  into `exam`(`id`,`name`,`room`,`time`,`invigilator`,`state`,`course_id`,`duration`) values 
(1,'C语言考试','一教1103','2023-05-23 18:49','刘松','进行中',1,150),
(2,'Java考试','一教1205','2023-05-10 09:00','张军','已结束',2,150),
(3,'Python考试','一教1508','2023-05-20 14:30','王波','已结束',3,150);

/*Table structure for table `files` */

DROP TABLE IF EXISTS `files`;

CREATE TABLE `files` (
  `id` int(10) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `name` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '文件名称',
  `type` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '文件类型',
  `size` bigint(20) DEFAULT NULL COMMENT '文件大小',
  `url` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '下载链接',
  `enable` tinyint(1) DEFAULT '1' COMMENT '是否禁用链接',
  `md5` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '文件MD5',
  PRIMARY KEY (`id`),
  UNIQUE KEY `uni_md5` (`md5`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

/*Data for the table `files` */

insert  into `files`(`id`,`name`,`type`,`size`,`url`,`enable`,`md5`) values 
(1,'软考证书','pdf',344,'http://localhost:8888/files/软考证书.pdf',1,'46ad5e07b8eb3566119612616acd52ef'),
(2,'导入模版','xls',30,'http://localhost:8888/files/导入模版.xls',0,'539db1ec96f4e4344a4c6de52d7c8682');

/*Table structure for table `p_q` */

DROP TABLE IF EXISTS `p_q`;

CREATE TABLE `p_q` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `paper_id` int(11) DEFAULT NULL COMMENT '试卷id',
  `question_id` int(11) DEFAULT NULL COMMENT '题目id',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=85 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

/*Data for the table `p_q` */

insert  into `p_q`(`id`,`paper_id`,`question_id`) values 
(27,2,38),
(28,2,41),
(29,2,42),
(30,2,29),
(31,2,28),
(32,2,37),
(33,2,34),
(34,2,33),
(35,2,30),
(36,2,40),
(37,2,45),
(38,2,49),
(39,2,51),
(40,2,44),
(41,2,46),
(42,2,53),
(43,2,56),
(44,2,55),
(45,3,57),
(46,3,58),
(47,3,59),
(48,3,60),
(49,3,61),
(50,3,62),
(51,3,63),
(52,3,64),
(53,3,65),
(54,3,66),
(55,3,67),
(56,3,68),
(57,3,69),
(58,3,70),
(59,3,71),
(60,3,72),
(61,3,73),
(62,3,74),
(63,3,75),
(64,3,76),
(65,3,77),
(66,3,78),
(67,1,8),
(68,1,3),
(69,1,10),
(70,1,6),
(71,1,2),
(72,1,4),
(73,1,1),
(74,1,7),
(75,1,9),
(76,1,11),
(77,1,18),
(78,1,12),
(79,1,20),
(80,1,19),
(81,1,17),
(82,1,25),
(83,1,22),
(84,1,26);

/*Table structure for table `paper` */

DROP TABLE IF EXISTS `paper`;

CREATE TABLE `paper` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '试卷名称',
  `duration` int(11) DEFAULT NULL COMMENT '考试时长',
  `course_id` int(11) DEFAULT NULL COMMENT '课程id',
  `type1` int(11) DEFAULT NULL COMMENT '选择题数量',
  `type2` int(11) DEFAULT NULL COMMENT '判断题数量',
  `type3` int(11) DEFAULT NULL COMMENT '问答题数量',
  `paper_id` int(11) DEFAULT NULL COMMENT '试卷id',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

/*Data for the table `paper` */

insert  into `paper`(`id`,`name`,`duration`,`course_id`,`type1`,`type2`,`type3`,`paper_id`) values 
(1,'C语言试题',150,1,NULL,NULL,NULL,NULL),
(2,'Java试题',150,2,NULL,NULL,NULL,NULL),
(3,'Python试题',150,3,NULL,NULL,NULL,NULL);

/*Table structure for table `question` */

DROP TABLE IF EXISTS `question`;

CREATE TABLE `question` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '题目名称',
  `type` int(11) DEFAULT NULL COMMENT '题目类型:1.选择、2.判断、3.问答',
  `a` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '选项A',
  `b` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '选项B',
  `c` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '选项C',
  `d` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '选项D',
  `score` int(10) DEFAULT NULL COMMENT '分数',
  `answer` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '答案',
  `detail` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '解析',
  `teacher_name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT '出题人',
  `time` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '出题时间',
  `course_id` int(11) DEFAULT NULL COMMENT '课程id',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=79 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

/*Data for the table `question` */

insert  into `question`(`id`,`name`,`type`,`a`,`b`,`c`,`d`,`score`,`answer`,`detail`,`teacher_name`,`time`,`course_id`) values 
(1,'C语言源程序的基本单位是（ ）',1,'过程','函数','子程序','标识符',5,'B','C语言源程序的基本单位是函数','星空之尘','2023-04-20 23:22:38',1),
(2,'C语言程序的三种基本结构是（ ）',1,'顺序结构、选择结构、循环结构','递归结构、循环结构、转移结构','嵌套结构、递归结构、循环结构','循环结构、转移结构、顺序结构',5,'A','','星空之尘','2023-04-20 23:44:35',1),
(3,'合法的数组定义是（ ）',1,'int a[] = \"string\"','int a[5] = {0,1,2,3,4,5}','char s = \"string\"','char a[] = {0,1,2,3,4,5}',5,'D','','星空之尘','2023-04-20 23:45:23',1),
(4,'为了避免嵌套的条件语句if else的二义性，C语言规定: else 与（ ）配对。',1,'缩排位置相同的if','其之前最近的未匹配的if','其之后最近的if','同一行的if',5,'B',NULL,'星空之尘','2023-04-20 23:51:32',1),
(5,'数组名作为参数传递给函数，作为该参数的数组名被处理为（ ）',1,'该数组的长度','该数组的元素个数','该数组中元素的值','该数组的首地址',5,'D',NULL,'星空之尘','2023-04-20 23:53:06',1),
(6,'一个可执行的C程序的开始执行点是（ ）',1,'程序中第一个语句','包含文件的第一个函数','main函数','程序中的第一个函数',5,'C',NULL,'星空之尘','2023-04-20 23:57:35',1),
(7,'C语言用（ ）表示逻辑“真”值。',1,'true','1','非0整数值','整数0',5,'B',NULL,'星空之尘','2023-04-20 23:57:37',1),
(8,'运行完下面的C语言程序段后，k的值是（ ）。\nint k = 4;\n(k++) + (k++) + (k++);',1,'4','5','6','7',5,'D',NULL,'星空之尘','2023-04-20 23:58:01',1),
(9,'在C语言中，提供的合法保留字是（ ）',1,'switch','cher','Case','default',5,'D',NULL,'星空之尘','2023-04-20 23:58:04',1),
(10,'若有定义char s[10] = \"abcd\"，则语句printf(\"%s,s\")的输出结果是（ ）（u表示空格）',1,'abcd','a','abcduuuuuuu','s',5,'A',NULL,'星空之尘','2023-04-20 23:58:08',1),
(11,'在C语言中，合法的字符常量是（ ）',1,'\'\\084\'','\'\\x43\'','\'ab\'','\'\\0\'',5,'B',NULL,'星空之尘','2023-04-20 23:58:16',1),
(12,'C语言是以函数为程序的基本单位的。',2,NULL,NULL,NULL,NULL,1,'√',NULL,'星空之尘','2023-04-20 23:58:19',1),
(13,'字符串常量是用一对双引号括起来的一个字符',2,NULL,NULL,NULL,NULL,1,'×',NULL,'星空之尘','2023-04-20 23:58:22',1),
(14,'字符串的结束标志是\'\\0\'。',2,NULL,NULL,NULL,NULL,1,'√',NULL,'星空之尘','2023-04-20 23:58:25',1),
(15,'对于“do {循环体} while (循环条件);”， 这个循环结构有可能一次循环体的内容都无法执行。',2,NULL,NULL,NULL,NULL,1,'×',NULL,'星空之尘','2023-04-20 23:58:29',1),
(16,'example和Example是两个完全不同的标识符。',2,NULL,NULL,NULL,NULL,1,'√','','星空之尘','2023-04-20 23:58:31',1),
(17,'C语言中，若函数说明未给出返回值类型，系统默认为void类型。',2,NULL,NULL,NULL,NULL,1,'×',NULL,'星空之尘','2023-04-20 23:58:35',1),
(18,'C语言本身不提供输入、输入语句，需而要由库函数给出。',2,NULL,NULL,NULL,NULL,1,'√',NULL,'星空之尘','2023-04-20 23:58:38',1),
(19,'在C语言中，有调用关系的所有函数必须放在同一个源程序文件中。',2,'','','','',1,'×',NULL,'星空之尘','2023-04-23 23:42:49',1),
(20,'C语言中，32768是不正确的int类型常数。',2,NULL,NULL,NULL,NULL,1,'√',NULL,'星空之尘','2023-04-28 21:20:18',1),
(21,'在一个函数内定义的变量只能在本函数范围内有效。',2,NULL,NULL,NULL,NULL,1,'√',NULL,'星空之尘','2023-04-29 21:55:36',1),
(22,'请描述C语言的优势，并简述C语言的未来发展方向。',3,NULL,NULL,NULL,NULL,15,NULL,NULL,'星空之尘','2023-04-30 00:03:43',1),
(23,'编写九九乘法表的代码。',3,NULL,NULL,NULL,NULL,20,NULL,NULL,'星空之尘','2023-05-02 20:45:23',1),
(24,'C语言中，基本数据类型有几个，请分别加以描述。',3,NULL,NULL,NULL,NULL,10,NULL,NULL,'星空之尘','2023-05-15 17:35:48',1),
(25,'头文件中的ifndef/define/endif 干什么用？',3,NULL,NULL,NULL,NULL,5,'防止该头文件被重复引用。',NULL,'星空之尘','2023-05-15 17:38:23',1),
(26,'C语言起源于哪一年，C语言的发明者是谁?',3,NULL,NULL,NULL,NULL,6,NULL,NULL,'星空之尘','2023-05-15 17:42:16',1),
(27,'编译Java Application 源程序文件将产生相应的字节码文件，这些字节码文件的扩展名为（ ）。',1,'java','.class','html','.exe',5,'B',NULL,'星空之尘','2023-05-15 17:42:56',2),
(28,'设x=1，y=2，z=3，则表达式y+=z--/++x 的值是（ ）。',1,'3','3.5','4','5',5,'A',NULL,'星空之尘','2023-05-15 17:44:55',2),
(29,'不允许作为类及类成员的访问控制符的是（ ）。',1,'public','private','static','protected',5,'C',NULL,'星空之尘','2023-05-15 17:46:07',2),
(30,'为AB类的一个无形式参数无返回值的方法method书写方法头，使得使用类名AB作为前缀就可以调用它，该方法头的形式为（ ）。',1,'static void method( )','public void method( )','final void method( )','abstract void method( )',5,'A',NULL,'星空之尘','2023-05-15 17:49:26',2),
(31,'Java application中的主类需包含main方法，以下哪项是main方法的正确形参？（ ）',1,'String args','String ar[]','Chart arg','StringBuffer args[]',5,'B',NULL,'星空之尘','2023-05-15 17:51:56',2),
(32,'以下关于继承的叙述正确的是（ ）。',1,'在Java中类只允许单一继承','在Java中一个类只能实现一个接口','在Java中一个类不能同时继承一个类和实现一个接口','在Java中接口只允许单一继承',5,'A',NULL,'星空之尘','2023-05-15 17:54:12',2),
(33,'paint()方法使用哪种类型的参数？（ ）',1,'Graphics','Graphics2D','String','Color',5,'A',NULL,'星空之尘','2023-05-15 17:55:40',2),
(34,'以下哪个不是Java的原始数据类型（ ）',1,'int','Boolean','float','char',5,'B',NULL,'星空之尘','2023-05-15 19:13:56',2),
(35,'以下哪项可能包含菜单条（ ）',1,'Panel','Frame','Applet','Dialog',5,'B',NULL,'星空之尘','2023-05-15 19:14:50',2),
(36,'若需要定义一个类域或类方法，应该使用哪种修饰符？（ ）',1,'static','package','private','public',5,'A',NULL,'星空之尘','2023-05-15 19:16:08',2),
(37,'在浏览器中执行applet程序，以下选项中的哪个方法将被最先执行（ ）。',1,'init()','start()','destroy()','stop()',5,'A',NULL,'星空之尘','2023-05-15 19:17:29',2),
(38,'给出下面代码，关于该程序以下哪个说法是正确的？（ ）\npublic class Person{\n    static int arr[] = new int[5];\n    public static void main(String a[]){\n        System.out.println(arr[0]);\n    }\n}',1,'编译时将产生错误','编译时正确，运行时将产生错误','输出零','输出空',5,'C',NULL,'星空之尘','2023-05-15 19:20:52',2),
(39,'下列哪些语句关于Java内存回收的说明是正确的？（ ）',1,'程序员必须创建一个线程来释放内存','内存回收程序负责释放无用内存','内存回收程序允许程序员直接释放内存','内存回收程序可以在指定的时间释放内存对象',5,'B',NULL,'星空之尘','2023-05-15 19:22:50',2),
(40,'以下哪个关键字可以用来对对象加互斥锁？（ ）',1,'transient','synchronized','serialize','static',5,'B',NULL,'星空之尘','2023-05-15 19:24:26',2),
(41,'以下代码段执行后的输出结果为（ ）\nint x = -3;\nint y = -10;\nSystem.out.println(y%x);',1,'-1','2','1','3',5,'A',NULL,'星空之尘','2023-05-15 19:25:47',2),
(42,'有以下程序片段，下列哪个选项不能插入到行1.（ ）\n1.\n2.public class Interesting{\n3.//do sth\n4.}',1,'import java.awt.*;','package mypackage;','class OtherClass{ }','public class MyClass{ }',5,'D',NULL,'星空之尘','2023-05-15 19:28:41',2),
(43,'Java的源代码中定义几个类，编译结果就生成几个以.class为后缀的字节码文件。',2,NULL,NULL,NULL,NULL,1,'√',NULL,'星空之尘','2023-05-15 19:30:54',2),
(44,'Java程序里，创建新的类对象用关键字new，回收无用的类对象使用关键字free。',2,NULL,NULL,NULL,NULL,1,'×',NULL,'星空之尘','2023-05-15 19:31:48',2),
(45,'Java有垃圾回收机制，内存回收程序可在指定的时间释放内存对象。',2,NULL,NULL,NULL,NULL,1,'×',NULL,'星空之尘','2023-05-15 19:32:31',2),
(46,'构造函数用于创建类的实例对象，构造函数名应与类名相同，返回类型为void。',2,NULL,NULL,NULL,NULL,1,'×',NULL,'星空之尘','2023-05-15 19:33:45',2),
(47,'在异常处理中，若try中的代码可能产生多种异常则可以对应多个catch语句，若catch中的参数类型中有父类子类关系，此时应该将父类放在后面，子类放在前面。',2,NULL,NULL,NULL,NULL,1,'√',NULL,'星空之尘','2023-05-15 19:35:15',2),
(48,'拥有abstract方法的类是抽象类，但抽象类中可以没有abstract方法',2,NULL,NULL,NULL,NULL,1,'√',NULL,'星空之尘','2023-05-15 19:36:14',2),
(49,'Java的屏幕坐标是以像素为单位，容器的左下角被确定为坐标的起点。',2,NULL,NULL,NULL,NULL,1,'×',NULL,'星空之尘','2023-05-15 19:36:52',2),
(50,'静态初始化器是在其所属的类加载内存时由系统自动调用执行。',2,NULL,NULL,NULL,NULL,1,'√',NULL,'星空之尘','2023-05-15 19:37:36',2),
(51,'在Java中对象可以赋值，只要使用赋值号（等号）即可，相当于生成了一个各属性与赋值对象相同的新对象。',2,NULL,NULL,NULL,NULL,1,'×',NULL,'星空之尘','2023-05-15 19:39:00',2),
(52,'什么是对象序列化，为什么要使用？',3,NULL,NULL,NULL,NULL,6,'所谓对象序列化就是把一个对象以二进制流的方式保存到硬盘上。好处：方便远程调用。',NULL,'星空之尘','2023-05-15 19:40:59',2),
(53,'值传递与引用传递的区别？',3,NULL,NULL,NULL,NULL,6,'所谓值传递就是把一个对象的值传给一个新的变量，但是系统会给这个新的变量开的一个新的内存空间，不会改变原有的值。\n所谓引用传递就是把一个对象在堆中保存的数据传递给二个变量，此时新的变量与原有的变量对应同一个内存存储空间，当新的变量修改对象的属性时，内存中的数据也会修改。',NULL,'星空之尘','2023-05-15 19:42:52',2),
(54,'谈谈你对面向对象的理解与认识？',3,NULL,NULL,NULL,NULL,20,NULL,NULL,'星空之尘','2023-05-15 19:43:57',2),
(55,'谈谈继承，为什么要使用继承？',3,NULL,NULL,NULL,NULL,10,'所谓继承就是找出几个类中共同的部分，提取出来作为父类。而子类只需要继承父类，就可以共享父类的方法。使用继承能够减少重复的代码。\n',NULL,'星空之尘','2023-05-15 19:44:57',2),
(56,'方法重载的好处?',3,NULL,NULL,NULL,NULL,10,'所谓重载就是在一个类中可以定义多个相同的方法，但是方法的参数类型和参数的个数以及顺序要不同。重载的好处就是能够让我们很快的掌握该方法的功能,我们只要记住该方法就能很快的理解该方法的参数以及参数的作用。',NULL,'星空之尘','2023-05-15 19:45:54',2),
(57,'下面不属于Python特性的是（ ）。',1,'简单易学','开源的免费的','属于低级语言','高可移植性',3,'C',NULL,'星空之尘','2023-05-15 19:51:29',3),
(58,'Python脚本文件的扩展名为（ ）。',1,'.python','.py','.pt','.pg',3,'B',NULL,'星空之尘','2023-05-15 19:52:34',3),
(59,'当需要在字符串中使用特殊字符时，Python使用（ ）作为转义字符。',1,'\\','/','#','%',3,'A',NULL,'星空之尘','2023-05-15 19:53:36',3),
(60,'下面（ ）不是有效的变量名。',1,'_demo','banana','Numbr','my-score',NULL,'D',NULL,'星空之尘','2023-05-15 19:54:41',3),
(61,'幂运算运算符为（ ）。',1,'*','**','%','//',3,'B',NULL,'星空之尘','2023-05-15 19:55:27',3),
(62,'关于a or b的描述错误的是（ ）。',1,'若a=True，b=True，则a or b == True','若a=True，b=False，则a or b == True','若a=True，b=True，则a or b == False','若a=False，b=False，则a or b == False',3,'C',NULL,'星空之尘','2023-05-15 19:57:23',3),
(63,'优先级最高的运算符为（ ）。',1,'/','//','*','()',3,'D',NULL,'星空之尘','2023-05-15 19:58:05',3),
(64,'使用（ ）关键字来创建python自定义函数。',1,'function','func','procedure','def',3,'D',NULL,'星空之尘','2023-05-15 19:59:02',3),
(65,'下面程序的运行结果为（ ）。\na=10\ndef setNumber();\n    a=100\nsetNumber()\nprint(a)',1,'10','100','10100','10010',3,'A',NULL,'星空之尘','2023-05-15 20:00:48',3),
(66,'（ ）模块是python标准库中最常用的模块之一。通过它可以获取命令行参数，从而实现从程序外部向程序内部传递参数的功能，也可以获取程序路径和当前系统平台等信息。',1,'sys','platform','math','time',3,'A',NULL,'星空之尘','2023-05-15 20:02:05',3),
(67,'（ ）不是用于处理中文的字符编码。',1,'gb2312','gbk','big5','ascii',3,'D',NULL,'星空之尘','2023-05-15 20:03:10',3),
(68,'关于赋值语句的作用，正确的描述是',1,'变量和对象必须类型相同','每个赋值语句只能给一个变量赋值','将变量改写为新的值','将变量绑定到对象',3,'D',NULL,'星空之尘','2023-05-15 20:04:00',3),
(69,'获得字符串s长度的方法是什么？',1,'s.len()','s.length','len(s)','length(s)',3,'C',NULL,'星空之尘','2023-05-15 20:04:56',3),
(70,'Python是一种跨平台、开源、免费的高级动态编程语言。',2,NULL,NULL,NULL,NULL,1,'√',NULL,'星空之尘','2023-05-15 20:05:54',3),
(71,'Python 3.x完全兼容Python 2.x。',2,NULL,NULL,NULL,NULL,1,'×',NULL,'星空之尘','2023-05-15 20:06:17',3),
(72,'Python 3.x和Python 2.x唯一的区别就是：print在Python 2.x中是输出语句，而在Python 3.x中是输出函数。',2,NULL,NULL,NULL,NULL,1,'×',NULL,'星空之尘','2023-05-15 20:07:01',3),
(73,'在Windows平台上编写的Python程序无法在Unix平台运行。',2,NULL,NULL,NULL,NULL,1,'×',NULL,'星空之尘','2023-05-15 20:07:23',3),
(74,'不可以在同一台计算机上安装多个Python版本。',2,NULL,NULL,NULL,NULL,1,'×',NULL,'星空之尘','2023-05-15 20:07:55',3),
(75,'已知X=3，那么赋值语句X=\'abcedfg\'是无法正赏执行的。',2,NULL,NULL,NULL,NULL,1,'×',NULL,'星空之尘','2023-05-15 20:08:55',3),
(76,'简述Python的特点',3,NULL,NULL,NULL,NULL,6,NULL,NULL,'星空之尘','2023-05-15 20:09:33',3),
(77,'什么是Python？使用Python有什么好处？',3,NULL,NULL,NULL,NULL,10,NULL,NULL,'星空之尘','2023-05-15 20:10:05',3),
(78,'有哪些工具可以帮助debug或做静态分析？',3,NULL,NULL,NULL,NULL,20,NULL,NULL,'星空之尘','2023-05-15 20:10:34',3);

/*Table structure for table `s_p` */

DROP TABLE IF EXISTS `s_p`;

CREATE TABLE `s_p` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `exam_id` int(11) DEFAULT NULL COMMENT '考试id',
  `paper` text COLLATE utf8mb4_bin COMMENT '试卷内容',
  `student_id` int(11) DEFAULT NULL COMMENT '学生id',
  `time` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '提交时间',
  `score` int(11) DEFAULT '0' COMMENT '得分',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

/*Data for the table `s_p` */

insert  into `s_p`(`id`,`exam_id`,`paper`,`student_id`,`time`,`score`) values 
(3,1,'[{\"id\":1,\"name\":\"C语言源程序的基本单位是（ ）\",\"type\":1,\"a\":\"过程\",\"b\":\"函数\",\"c\":\"子程序\",\"d\":\"标识符\",\"score\":5,\"answer\":\"B\",\"detail\":\"C语言源程序的基本单位是函数\",\"teacherName\":\"星空之尘\",\"time\":\"2023-04-20 23:22:38\",\"courseId\":\"1\",\"studentAnswer\":\"A\"},{\"id\":2,\"name\":\"C语言程序的三种基本结构是（ ）\",\"type\":1,\"a\":\"顺序结构、选择结构、循环结构\",\"b\":\"递归结构、循环结构、转移结构\",\"c\":\"嵌套结构、递归结构、循环结构\",\"d\":\"循环结构、转移结构、顺序结构\",\"score\":5,\"answer\":\"A\",\"detail\":\"\",\"teacherName\":\"星空之尘\",\"time\":\"2023-04-20 23:44:35\",\"courseId\":\"1\",\"studentAnswer\":\"A\"},{\"id\":3,\"name\":\"合法的数组定义是（ ）\",\"type\":1,\"a\":\"int a[] = \\\"string\\\"\",\"b\":\"int a[5] = {0,1,2,3,4,5}\",\"c\":\"char s = \\\"string\\\"\",\"d\":\"char a[] = {0,1,2,3,4,5}\",\"score\":5,\"answer\":\"D\",\"detail\":\"\",\"teacherName\":\"星空之尘\",\"time\":\"2023-04-20 23:45:23\",\"courseId\":\"1\",\"studentAnswer\":\"A\"},{\"id\":4,\"name\":\"为了避免嵌套的条件语句if else的二义性，C语言规定: else 与（ ）配对。\",\"type\":1,\"a\":\"缩排位置相同的if\",\"b\":\"其之前最近的未匹配的if\",\"c\":\"其之后最近的if\",\"d\":\"同一行的if\",\"score\":5,\"answer\":\"B\",\"detail\":null,\"teacherName\":\"星空之尘\",\"time\":\"2023-04-20 23:51:32\",\"courseId\":\"1\",\"studentAnswer\":\"A\"},{\"id\":5,\"name\":\"数组名作为参数传递给函数，作为该参数的数组名被处理为（ ）\",\"type\":1,\"a\":\"该数组的长度\",\"b\":\"该数组的元素个数\",\"c\":\"该数组中元素的值\",\"d\":\"该数组的首地址\",\"score\":5,\"answer\":\"D\",\"detail\":null,\"teacherName\":\"星空之尘\",\"time\":\"2023-04-20 23:53:06\",\"courseId\":\"1\",\"studentAnswer\":\"A\"},{\"id\":6,\"name\":\"一个可执行的C程序的开始执行点是（ ）\",\"type\":1,\"a\":\"程序中第一个语句\",\"b\":\"包含文件的第一个函数\",\"c\":\"main函数\",\"d\":\"程序中的第一个函数\",\"score\":5,\"answer\":\"C\",\"detail\":null,\"teacherName\":\"星空之尘\",\"time\":\"2023-04-20 23:57:35\",\"courseId\":\"1\",\"studentAnswer\":\"A\"},{\"id\":7,\"name\":\"C语言用（ ）表示逻辑“真”值。\",\"type\":1,\"a\":\"true\",\"b\":\"1\",\"c\":\"非0整数值\",\"d\":\"整数0\",\"score\":5,\"answer\":\"B\",\"detail\":null,\"teacherName\":\"星空之尘\",\"time\":\"2023-04-20 23:57:37\",\"courseId\":\"1\",\"studentAnswer\":\"A\"},{\"id\":8,\"name\":\"运行完下面的C语言程序段后，k的值是（ ）。\\nint k = 4;\\n(k++) + (k++) + (k++);\",\"type\":1,\"a\":\"4\",\"b\":\"5\",\"c\":\"6\",\"d\":\"7\",\"score\":5,\"answer\":\"D\",\"detail\":null,\"teacherName\":\"星空之尘\",\"time\":\"2023-04-20 23:58:01\",\"courseId\":\"1\",\"studentAnswer\":\"A\"},{\"id\":9,\"name\":\"在C语言中，提供的合法保留字是（ ）\",\"type\":1,\"a\":\"switch\",\"b\":\"cher\",\"c\":\"Case\",\"d\":\"default\",\"score\":5,\"answer\":\"D\",\"detail\":null,\"teacherName\":\"星空之尘\",\"time\":\"2023-04-20 23:58:04\",\"courseId\":\"1\",\"studentAnswer\":\"A\"},{\"id\":10,\"name\":\"若有定义char s[10] = \\\"abcd\\\"，则语句printf(\\\"%s,s\\\")的输出结果是（ ）（u表示空格）\",\"type\":1,\"a\":\"abcd\",\"b\":\"a\",\"c\":\"abcduuuuuuu\",\"d\":\"s\",\"score\":5,\"answer\":\"A\",\"detail\":null,\"teacherName\":\"星空之尘\",\"time\":\"2023-04-20 23:58:08\",\"courseId\":\"1\",\"studentAnswer\":\"A\"},{\"id\":11,\"name\":\"在C语言中，合法的字符常量是（ ）\",\"type\":1,\"a\":\"\'\\\\084\'\",\"b\":\"\'\\\\x43\'\",\"c\":\"\'ab\'\",\"d\":\"\'\\\\0\'\",\"score\":5,\"answer\":\"B\",\"detail\":null,\"teacherName\":\"星空之尘\",\"time\":\"2023-04-20 23:58:16\",\"courseId\":\"1\",\"studentAnswer\":\"A\"},{\"id\":12,\"name\":\"C语言是以函数为程序的基本单位的。\",\"type\":2,\"a\":null,\"b\":null,\"c\":null,\"d\":null,\"score\":1,\"answer\":\"√\",\"detail\":null,\"teacherName\":\"星空之尘\",\"time\":\"2023-04-20 23:58:19\",\"courseId\":\"1\",\"studentAnswer\":\"√\"},{\"id\":13,\"name\":\"字符串常量是用一对双引号括起来的一个字符\",\"type\":2,\"a\":null,\"b\":null,\"c\":null,\"d\":null,\"score\":1,\"answer\":\"×\",\"detail\":null,\"teacherName\":\"星空之尘\",\"time\":\"2023-04-20 23:58:22\",\"courseId\":\"1\",\"studentAnswer\":\"√\"},{\"id\":14,\"name\":\"字符串的结束标志是\'\\\\0\'。\",\"type\":2,\"a\":null,\"b\":null,\"c\":null,\"d\":null,\"score\":1,\"answer\":\"√\",\"detail\":null,\"teacherName\":\"星空之尘\",\"time\":\"2023-04-20 23:58:25\",\"courseId\":\"1\",\"studentAnswer\":\"√\"},{\"id\":15,\"name\":\"对于“do {循环体} while (循环条件);”， 这个循环结构有可能一次循环体的内容都无法执行。\",\"type\":2,\"a\":null,\"b\":null,\"c\":null,\"d\":null,\"score\":1,\"answer\":\"×\",\"detail\":null,\"teacherName\":\"星空之尘\",\"time\":\"2023-04-20 23:58:29\",\"courseId\":\"1\",\"studentAnswer\":\"√\"},{\"id\":16,\"name\":\"example和Example是两个完全不同的标识符。\",\"type\":2,\"a\":null,\"b\":null,\"c\":null,\"d\":null,\"score\":1,\"answer\":\"√\",\"detail\":\"\",\"teacherName\":\"星空之尘\",\"time\":\"2023-04-20 23:58:31\",\"courseId\":\"1\",\"studentAnswer\":\"√\"},{\"id\":17,\"name\":\"C语言中，若函数说明未给出返回值类型，系统默认为void类型。\",\"type\":2,\"a\":null,\"b\":null,\"c\":null,\"d\":null,\"score\":1,\"answer\":\"×\",\"detail\":null,\"teacherName\":\"星空之尘\",\"time\":\"2023-04-20 23:58:35\",\"courseId\":\"1\",\"studentAnswer\":\"√\"},{\"id\":18,\"name\":\"C语言本身不提供输入、输入语句，需而要由库函数给出。\",\"type\":2,\"a\":null,\"b\":null,\"c\":null,\"d\":null,\"score\":1,\"answer\":\"√\",\"detail\":null,\"teacherName\":\"星空之尘\",\"time\":\"2023-04-20 23:58:38\",\"courseId\":\"1\",\"studentAnswer\":\"√\"},{\"id\":19,\"name\":\"在C语言中，有调用关系的所有函数必须放在同一个源程序文件中。\",\"type\":2,\"a\":\"\",\"b\":\"\",\"c\":\"\",\"d\":\"\",\"score\":1,\"answer\":\"×\",\"detail\":null,\"teacherName\":\"星空之尘\",\"time\":\"2023-04-23 23:42:49\",\"courseId\":\"1\",\"studentAnswer\":\"√\"},{\"id\":20,\"name\":\"C语言中，32768是不正确的int类型常数。\",\"type\":2,\"a\":null,\"b\":null,\"c\":null,\"d\":null,\"score\":1,\"answer\":\"√\",\"detail\":null,\"teacherName\":\"星空之尘\",\"time\":\"2023-04-28 21:20:18\",\"courseId\":\"1\",\"studentAnswer\":\"√\"},{\"id\":21,\"name\":\"在一个函数内定义的变量只能在本函数范围内有效。\",\"type\":2,\"a\":null,\"b\":null,\"c\":null,\"d\":null,\"score\":1,\"answer\":\"√\",\"detail\":null,\"teacherName\":\"星空之尘\",\"time\":\"2023-04-29 21:55:36\",\"courseId\":\"1\",\"studentAnswer\":\"√\"},{\"id\":22,\"name\":\"请描述C语言的优势，并简述C语言的未来发展方向。\",\"type\":3,\"a\":null,\"b\":null,\"c\":null,\"d\":null,\"score\":15,\"answer\":null,\"detail\":null,\"teacherName\":\"星空之尘\",\"time\":\"2023-04-30 00:03:43\",\"courseId\":\"1\"},{\"id\":23,\"name\":\"编写九九乘法表的代码。\",\"type\":3,\"a\":null,\"b\":null,\"c\":null,\"d\":null,\"score\":20,\"answer\":null,\"detail\":null,\"teacherName\":\"星空之尘\",\"time\":\"2023-05-02 20:45:23\",\"courseId\":\"1\"},{\"id\":24,\"name\":\"C语言中，基本数据类型有几个，请分别加以描述。\",\"type\":3,\"a\":null,\"b\":null,\"c\":null,\"d\":null,\"score\":10,\"answer\":null,\"detail\":null,\"teacherName\":\"星空之尘\",\"time\":\"2023-05-15 17:35:48\",\"courseId\":\"1\"},{\"id\":25,\"name\":\"头文件中的ifndef/define/endif 干什么用？\",\"type\":3,\"a\":null,\"b\":null,\"c\":null,\"d\":null,\"score\":5,\"answer\":\"防止该头文件被重复引用。\",\"detail\":null,\"teacherName\":\"星空之尘\",\"time\":\"2023-05-15 17:38:23\",\"courseId\":\"1\"},{\"id\":26,\"name\":\"C语言起源于哪一年，C语言的发明者是谁?\",\"type\":3,\"a\":null,\"b\":null,\"c\":null,\"d\":null,\"score\":6,\"answer\":null,\"detail\":null,\"teacherName\":\"星空之尘\",\"time\":\"2023-05-15 17:42:16\",\"courseId\":\"1\"}]',1,'2023-05-16 23:37:58',16);

/*Table structure for table `sign` */

DROP TABLE IF EXISTS `sign`;

CREATE TABLE `sign` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `exam_id` int(11) NOT NULL COMMENT '考试id',
  `student_id` int(11) NOT NULL COMMENT '学生id',
  `state` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT '待审核' COMMENT '审核状态',
  PRIMARY KEY (`id`),
  UNIQUE KEY `exam_student_index` (`exam_id`,`student_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `sign` */

insert  into `sign`(`id`,`exam_id`,`student_id`,`state`) values 
(1,1,1,'审核已通过'),
(2,2,1,'审核已通过'),
(3,3,1,'审核已通过');

/*Table structure for table `student` */

DROP TABLE IF EXISTS `student`;

CREATE TABLE `student` (
  `id` int(10) NOT NULL AUTO_INCREMENT COMMENT '学生id',
  `username` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT '学生用户名',
  `password` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT '学生密码',
  `nickname` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT '学生昵称',
  `email` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT '学生邮箱',
  `phone` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT '学生电话',
  `address` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT '学生地址',
  `signature` varchar(600) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '学生签名',
  `create_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP COMMENT '注册时间',
  `token` varchar(500) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '学生token',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

/*Data for the table `student` */

insert  into `student`(`id`,`username`,`password`,`nickname`,`email`,`phone`,`address`,`signature`,`create_time`,`token`) values 
(1,'杨子宇','190517501008','Mr.杨','1428010141@qq.com','18848588783','贵州省剑河县南加镇城东社区街上一组','总有地上的生灵，敢于直面雷霆的威光。','2023-04-24 22:24:24',NULL);

/*Table structure for table `teacher` */

DROP TABLE IF EXISTS `teacher`;

CREATE TABLE `teacher` (
  `tid` int(10) NOT NULL AUTO_INCREMENT COMMENT '教师id',
  `username` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT '教师用户名',
  `password` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT '教师密码',
  `nickname` varchar(30) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '教师昵称',
  `email` varchar(50) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '教师邮箱',
  `phone` varchar(20) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '教师电话',
  `address` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '教师地址',
  `str_sign` varchar(600) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '教师签名',
  `create_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP COMMENT '注册时间',
  `token` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT '教师token',
  PRIMARY KEY (`tid`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

/*Data for the table `teacher` */

insert  into `teacher`(`tid`,`username`,`password`,`nickname`,`email`,`phone`,`address`,`str_sign`,`create_time`,`token`) values 
(1,'星空之尘','18848588783','Startdust','2259315153@qq.com','1008611','贵阳学院','众星因你，皆降为尘。','2023-04-21 21:03:16',NULL);

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
