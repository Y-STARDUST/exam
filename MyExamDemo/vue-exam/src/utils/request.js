import axios from 'axios'
import ElementUI from "element-ui";

const request = axios.create({
    baseURL: 'http://localhost:8888',
    timeout: 5000
})

// request 拦截器
// 可以自请求发送前对请求做一些处理
request.interceptors.request.use(config => {
    config.headers['Content-Type'] = 'application/json;charset=utf-8';

    let teacher = localStorage.getItem("Teacher") ? JSON.parse(localStorage.getItem("Teacher")) : null
    let student = localStorage.getItem("Teacher") ? JSON.parse(localStorage.getItem("Teacher")) : null

    if (teacher) {
        config.headers['Tea-token'] = teacher.token
    }
    if (student) {
        config.headers['Stu-token'] = student.token
    }

    return config
}, error => {
    return Promise.reject(error)
});

// response 拦截器
// 可以在接口响应后统一处理结果
request.interceptors.response.use(
    response => {
        let res = response.data;
        // 如果是返回的文件
        if (response.config.responseType === 'blob') {
            return res
        }
        // 兼容服务端返回的字符串数据
        if (typeof res === 'string') {
            res = res ? JSON.parse(res) : res
        }
        if (res.code === "code_401") {
            ElementUI.Message({
                message: res.msg,
                type: 'error'
            })
        }
        return res;
    },
    error => {
        console.log('err' + error) // for debug
        return Promise.reject(error)
    }
)


export default request

