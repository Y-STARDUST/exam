import Vue from 'vue'
import VueRouter from 'vue-router'
import store from "@/store";

Vue.use(VueRouter)

const routes = [
    {
        path: '/',
        name: '首页',
        component: () => import('../views/Home')
    },
    {
        path: '/StuForgotPassword',
        name: '学生忘记密码',
        component: () => import('../views/StuForgotPassword')
    },
    {
        path: '/StuResetPassword',
        name: '学生修改密码',
        component: () => import('../views/StuResetPassword')
    },
    {
        path: '/TeaForgotPassword',
        name: '教师忘记密码',
        component: () => import('../views/TeaForgotPassword')
    },
    {
        path: '/System_StuExam',
        name: '学生端考试系统',
        component: () => import('../views/System_StuExam')
    },
    {
        path: '/Stu_PersonalCenter',
        name: '学生个人中心',
        component: () => import('../views/Stu_PersonalCenter')
    },
    {
        path: '/Stu_ExamPaper',
        name: '学生考试试卷',
        component: () => import('../views/Stu_ExamPaper')
    },
    {
        path: '/StuLogin',
        name: '学生登录',
        component: () => import('../views/StuLogin')
    },
    {
        path: '/StuRegister',
        name: '学生注册',
        component: () => import('../views/StuRegister')
    },
    {
        path: '/TeaLogin',
        name: '教师登录',
        component: () => import('../views/TeaLogin')
    },
    {
        path: '/TeaRegister',
        name: '教师注册',
        component: () => import('../views/TeaRegister')
    },
    {
        path: '/TeaRegisterSuccess',
        name: '教师注册成功',
        component: () => import('../views/TeaRegisterSuccess')
    },
    {
        path: '/StuRegisterSuccess',
        name: '学生注册成功',
        component: () => import('../views/StuRegisterSuccess')
    },
    {
        path: '/System_TeaBackend',
        name: '教师端后台管理系统',
        component: () => import('../views/System_TeaBackend'),
        children: [
            {path: 'Management_Student', name: '学生管理', component: () => import('../views/Management_Student')},
            {path: 'Tea_ProjectIntroduction', name: '项目介绍', component: () => import('../views/Tea_ProjectIntroduction')},
            {path: 'Settings_Exterior', name: '外观', component: () => import('../views/Settings_Exterior')},
            {path: 'Settings_Language', name: '语言', component: () => import('../views/Settings_Language')},
            {path: 'Tea_PersonalCenter', name: '个人中心', component: () => import('../views/Tea_PersonalCenter')},
            {path: 'Management_File', name: '文件管理', component: () => import('../views/Management_File')},
            {path: 'Management_Question', name: '题目管理', component: () => import('../views/Management_Question')},
            {path: 'Management_Course', name: '课程管理', component: () => import('../views/Management_Course')},
            {path: 'Management_Exam', name: '考试管理', component: () => import('../views/Management_Exam')},
            {path: 'Management_Paper', name: '试卷管理', component: () => import('../views/Management_Paper')},
            {path: 'Management_Sign', name: '报名管理', component: () => import('../views/Management_Sign')},
            {path: 'Management_Marking', name: '阅卷管理', component: () => import('../views/Management_Marking')},
            {path: 'Welcome', name: '欢迎', component: () => import('../views/Welcome')},
            {path: 'TeaResetPassword', name: '修改密码', component: () => import('../views/TeaResetPassword')},
        ]
    },
    {
        path: '/Tea_CorrectingPapers',
        name: '阅卷',
        component: () => import('../views/Tea_CorrectingPapers')
    },
]

const router = new VueRouter({
    mode: 'history',
    base: process.env.BASE_URL,
    routes
})

router.beforeEach((to, from, next) => {
    localStorage.setItem("currentPathName",to.name)
    store.commit("setPath")
    next()
})

export default router
