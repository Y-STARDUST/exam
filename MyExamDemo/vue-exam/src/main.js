import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import ElementUI from 'element-ui';
import 'element-ui/lib/theme-chalk/index.css';
import '../src/assets/globe.css'
import request from "@/utils/request";
import i18n from "./lang";
import moment from "moment"

Vue.config.productionTip = false

Vue.use(ElementUI, {size:"mini"});
Vue.prototype.request = request
Vue.prototype.$moment = moment;

new Vue({
  router,
  store,
  i18n,
  render: h => h(App)
}).$mount('#app')
