import Vue from "vue";
import VueI18n from 'vue-i18n'
import Chinese from "./Chinese"
import English from "./English"
import Japanese from "./Japanese"
import Korean from "./Korean"
import Russian from "./Russian"
import German from "./German"
import French from "./French"

Vue.use(VueI18n);   // 全局注册国际化包

// 准备翻译的语言环境信息
const i18n = new VueI18n({
    // 当前语言包不存在时，默认设置为中文
    locale : localStorage.getItem('lang') || 'Chinese',
    fallbackLocale: 'Chinese',
    messages: {
        "Chinese": Chinese,
        "English": English,
        "Japanese": Japanese,
        "Korean": Korean,
        "Russian": Russian,
        "German": German,
        "French": French,
    }
});

export default i18n