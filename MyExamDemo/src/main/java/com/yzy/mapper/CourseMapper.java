package com.yzy.mapper;

import com.yzy.entity.Course;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 *
 * CourseMapper
 *
 * @author 杨子宇
 * @createTime 2023-04-21
 */
@Mapper
public interface CourseMapper extends BaseMapper<Course> {

}
