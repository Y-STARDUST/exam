package com.yzy.mapper;

import com.yzy.entity.SP;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 *
 * SPMapper
 *
 * @author 杨子宇
 * @createTime 2023-04-30
 */
@Mapper
public interface SPMapper extends BaseMapper<SP> {

}
