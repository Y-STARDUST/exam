package com.yzy.mapper;

import com.yzy.entity.Paper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 *
 * PaperMapper
 *
 * @author 杨子宇
 * @createTime 2023-04-21
 */
@Mapper
public interface PaperMapper extends BaseMapper<Paper> {

}
