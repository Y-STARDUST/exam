package com.yzy.mapper;

import com.yzy.entity.Exam;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 *
 * ExamMapper
 *
 * @author 杨子宇
 * @createTime 2023-04-21
 */
@Mapper
public interface ExamMapper extends BaseMapper<Exam> {

}
