package com.yzy.mapper;

import com.yzy.entity.EP;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 *
 * EPMapper
 *
 * @author 杨子宇
 * @createTime 2023-04-21
 */
@Mapper
public interface EPMapper extends BaseMapper<EP> {

}
