package com.yzy.mapper;

import com.yzy.entity.Files;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 *
 * FilesMapper
 *
 * @author 杨子宇
 * @createTime 2023-04-19
 */
@Mapper
public interface FilesMapper extends BaseMapper<Files> {

}
