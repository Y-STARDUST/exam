package com.yzy.mapper;

import com.yzy.controller.dto.StuResetPasswordDTO;
import com.yzy.entity.Student;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Update;

/**
 *
 * StudentMapper
 *
 * @author 杨子宇
 * @createTime 2023-04-18
 */
@Mapper
public interface StudentMapper extends BaseMapper<Student> {
    @Update("update student set password = #{newPassword} where username = #{username} and password = #{password}")
    int updatePassword(StuResetPasswordDTO stuResetPasswordDTO);
}
