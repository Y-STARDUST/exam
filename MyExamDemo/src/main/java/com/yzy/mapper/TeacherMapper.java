package com.yzy.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.yzy.controller.dto.TeaResetPasswordDTO;
import com.yzy.entity.Teacher;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Update;

/**
 * TeacherMapper
 *
 * @author 杨子宇
 * @createTime 2023-04-17
 */
@Mapper
public interface TeacherMapper extends BaseMapper<Teacher> {
    @Update("update teacher set password = #{newPassword} where username = #{username} and password = #{password}")
    int updatePassword(TeaResetPasswordDTO teaResetPasswordDTO);
}
