package com.yzy.mapper;

import com.yzy.entity.Question;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 *
 * QuestionMapper
 *
 * @author 杨子宇
 * @createTime 2023-04-20
 */
@Mapper
public interface QuestionMapper extends BaseMapper<Question> {

}
