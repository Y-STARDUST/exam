package com.yzy.mapper;

import com.yzy.entity.PQ;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.yzy.entity.Question;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 *
 * PQMapper
 *
 * @author 杨子宇
 * @createTime 2023-04-21
 */
@Mapper
public interface PQMapper extends BaseMapper<PQ> {

    List<Question> selectQuestions(Integer paperId);
}
