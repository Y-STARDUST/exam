package com.yzy.mapper;

import com.yzy.entity.Sign;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 *
 * SignMapper
 *
 * @author 杨子宇
 * @createTime 2023-04-25
 */
@Mapper
public interface SignMapper extends BaseMapper<Sign> {

}
