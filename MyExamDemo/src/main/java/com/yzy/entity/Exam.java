package com.yzy.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

/**
 * Exam表
 *
 * @author 杨子宇
 * @createTime 2023-04-21
 */
@Data
@TableName("exam")
public class Exam implements Serializable {


    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    /**
     * 考试名称
     */
    @TableField("name")
    private String name;

    /**
     * 考场
     */
    @TableField("room")
    private String room;

    /**
     * 考试时间
     */
    @TableField("time")
    private String time;

    /**
     * 监考老师
     */
    @TableField("invigilator")
    private String invigilator;

    /**
     * 考试状态
     */
    @TableField("state")
    private String state;

    /**
     * 课程id
     */
    @TableField("course_id")
    private String courseId;

    /**
     * 持续时间（分）
     */
    @TableField("duration")
    private Integer duration;

    @Getter
    @Setter
    @TableField(exist = false)
    private Boolean enable;

}
