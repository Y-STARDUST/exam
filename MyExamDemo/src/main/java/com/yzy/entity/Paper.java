package com.yzy.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;

/**
 * Paper表
 *
 * @author 杨子宇
 * @createTime 2023-04-21
 */
@Data
@TableName("paper")
public class Paper implements Serializable {


    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    /**
     * 试卷名称
     */
    @TableField("name")
    private String name;

    /**
     * 考试时长
     */
    @TableField("duration")
    private Integer duration;

    /**
     * 课程id
     */
    @TableField("course_id")
    private Integer courseId;

    /**
     * 选择题数量
     */
    @TableField("type1")
    private Integer type1;

    /**
     * 判断题数量
     */
    @TableField("type2")
    private Integer type2;

    /**
     * 问答题数量
     */
    @TableField("type3")
    private Integer type3;

    /**
     * 试卷id
     */
    @TableField("paper_id")
    private Integer paperId;
}
