package com.yzy.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;

/**
 * PQ表
 *
 * @author 杨子宇
 * @createTime 2023-04-21
 */
@Data
@TableName("p_q")
public class PQ implements Serializable {


    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    /**
     * 试卷id
     */
    @TableField("paper_id")
    private Integer paperId;

    /**
     * 题目id
     */
    @TableField("question_id")
    private Integer questionId;
}
