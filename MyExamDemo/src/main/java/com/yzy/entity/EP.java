package com.yzy.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;

/**
 * EP表
 *
 * @author 杨子宇
 * @createTime 2023-04-21
 */
@Data
@TableName("e_p")
public class EP implements Serializable {


    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    /**
     * 考试id
     */
    @TableField("exam_id")
    private Integer examId;

    /**
     * 试卷id
     */
    @TableField("paper_id")
    private Integer paperId;
}
