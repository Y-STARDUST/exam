package com.yzy.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;
import lombok.Data;

/**
 *
 * SP表
 *
 * @author 杨子宇
 * @createTime 2023-04-30
 */
@Data
  @TableName("s_p")
public class SP implements Serializable {


      @TableId(value = "id", type = IdType.AUTO)
      private Integer id;

      /**
     * 考试id
     */
      @TableField("exam_id")
    private Integer examId;

      /**
     * 试卷内容
     */
      @TableField("paper")
    private String paper;

      /**
     * 学生id
     */
      @TableField("student_id")
    private Integer studentId;

      /**
     * 提交时间
     */
      @TableField("time")
    private String time;

      /**
     * 得分
     */
      @TableField("score")
    private Integer score;
}
