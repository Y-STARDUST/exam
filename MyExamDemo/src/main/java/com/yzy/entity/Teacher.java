package com.yzy.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * Teacher表
 *
 * @author 杨子宇
 * @createTime 2023-04-17
 */
@Data
@TableName("teacher")
public class Teacher implements Serializable {

    /**
     * 教师id
     */
    @TableId(value = "tid", type = IdType.AUTO)
    private Integer tid;

    /**
     * 教师用户名
     */
    @TableField("username")
    private String username;

    /**
     * 教师密码
     */
    @TableField("password")
    private String password;

    /**
     * 教师昵称
     */
    @TableField("nickname")
    private String nickname;

    /**
     * 教师邮箱
     */
    @TableField("email")
    private String email;

    /**
     * 教师电话
     */
    @TableField("phone")
    private String phone;

    /**
     * 教师地址
     */
    @TableField("address")
    private String address;

    /**
     * 教师签名
     */
    @TableField("str_sign")
    private String strSign;

    /**
     * 注册时间
     */
    @TableField("create_time")
    private LocalDateTime createTime;

    /**
     * 教师token
     */
    @TableField("token")
    private String token;
}
