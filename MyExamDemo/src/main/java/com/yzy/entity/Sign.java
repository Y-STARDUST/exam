package com.yzy.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;

/**
 *
 * Sign表
 *
 * @author 杨子宇
 * @createTime 2023-04-25
 */
@Data
  @TableName("sign")
public class Sign implements Serializable {


      @TableId(value = "id", type = IdType.AUTO)
      private Integer id;

      /**
     * 考试id
     */
      @TableField("exam_id")
    private Integer examId;

      /**
     * 学生id
     */
      @TableField("student_id")
    private Integer studentId;

      /**
     * 审核状态
     */
      @TableField("state")
    private String state;
}
