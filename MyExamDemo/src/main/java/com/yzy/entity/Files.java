package com.yzy.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;

/**
 * Files表
 *
 * @author 杨子宇
 * @createTime 2023-04-19
 */
@Data
@TableName("files")
public class Files implements Serializable {

    /**
     * id
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    /**
     * 文件名称
     */
    @TableField("name")
    private String name;

    /**
     * 文件类型
     */
    @TableField("type")
    private String type;

    /**
     * 文件大小
     */
    @TableField("size")
    private Long size;

    /**
     * 下载链接
     */
    @TableField("url")
    private String url;

    /**
     * 是否禁用链接
     */
    @TableField("enable")
    private Boolean enable;

    /**
     * 文件MD5
     */
    @TableField("md5")
    private String md5;
}
