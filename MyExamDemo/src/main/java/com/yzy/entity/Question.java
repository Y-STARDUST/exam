package com.yzy.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;

/**
 * Question表
 *
 * @author 杨子宇
 * @createTime 2023-04-20
 */
@Data
@TableName("question")
public class Question implements Serializable {


    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    /**
     * 题目名称
     */
    @TableField("name")
    private String name;

    /**
     * 题目类型:1.选择、2.判断、3.问答
     */
    @TableField("type")
    private Integer type;

    /**
     * 选项A
     */
    @TableField("a")
    private String a;

    /**
     * 选项B
     */
    @TableField("b")
    private String b;

    /**
     * 选项C
     */
    @TableField("c")
    private String c;

    /**
     * 选项D
     */
    @TableField("d")
    private String d;

    /**
     * 分数
     */
    @TableField("score")
    private Integer score;

    /**
     * 答案
     */
    @TableField("answer")
    private String answer;

    /**
     * 解析
     */
    @TableField("detail")
    private String detail;

    /**
     * 出题人
     */
    @TableField("teacher_name")
    private String teacherName;

    /**
     * 出题时间
     */
    @TableField("time")
    private String time;

    /**
     * 课程id
     */
    @TableField("course_id")
    private String courseId;
}
