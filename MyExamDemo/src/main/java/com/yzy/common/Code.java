package com.yzy.common;

/**
 * @author 杨子宇
 */

public enum Code{
    // 成功
    code_200,
    // 参数错误
    code_400,
    // 权限不足
    code_401,
    // 系统错误
    code_500,
    // 其他业务异常
    code_600
}
