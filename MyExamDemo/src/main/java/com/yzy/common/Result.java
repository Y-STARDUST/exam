package com.yzy.common;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 接口统一返回包装类
 *
 * @author 杨子宇
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Result {
    private Code code;
    private String msg;
    private Object data;

    public static Result success() {
        return new Result(Code.code_200, "", null);
    }

    public static Result success(Object data) {
        return new Result(Code.code_200, "", data);
    }

    public static Result error(Code code, String msg) {
        return new Result(code, msg, null);
    }

    public static Result error() {
        return new Result(Code.code_500, "系统错误", null);
    }
}
