package com.yzy.service;

import com.yzy.entity.PQ;
import com.baomidou.mybatisplus.extension.service.IService;
import com.yzy.entity.Question;

import java.util.List;

/**
 *
 * PQService
 *
 * @author 杨子宇
 * @createTime 2023-04-21
 */
public interface IPQService extends IService<PQ> {
    List<Question> selectQuestions(Integer paperId);
}
