package com.yzy.service;

import com.yzy.entity.Course;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 *
 * CourseService
 *
 * @author 杨子宇
 * @createTime 2023-04-21
 */
public interface ICourseService extends IService<Course> {

}
