package com.yzy.service;

import com.yzy.entity.Sign;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 *
 * SignService
 *
 * @author 杨子宇
 * @createTime 2023-04-25
 */
public interface ISignService extends IService<Sign> {

}
