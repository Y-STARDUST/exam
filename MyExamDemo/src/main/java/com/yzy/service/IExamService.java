package com.yzy.service;

import com.yzy.entity.Exam;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 *
 * ExamService
 *
 * @author 杨子宇
 * @createTime 2023-04-21
 */
public interface IExamService extends IService<Exam> {

}
