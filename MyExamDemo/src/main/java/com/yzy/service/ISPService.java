package com.yzy.service;

import com.yzy.entity.SP;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 *
 * SPService
 *
 * @author 杨子宇
 * @createTime 2023-04-30
 */
public interface ISPService extends IService<SP> {

}
