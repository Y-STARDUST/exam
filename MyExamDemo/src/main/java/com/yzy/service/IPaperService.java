package com.yzy.service;

import com.yzy.entity.Paper;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 *
 * PaperService
 *
 * @author 杨子宇
 * @createTime 2023-04-21
 */
public interface IPaperService extends IService<Paper> {

}
