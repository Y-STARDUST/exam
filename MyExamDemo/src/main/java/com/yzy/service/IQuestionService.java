package com.yzy.service;

import com.yzy.entity.Question;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 *
 * QuestionService
 *
 * @author 杨子宇
 * @createTime 2023-04-20
 */
public interface IQuestionService extends IService<Question> {

}
