package com.yzy.service;

import com.yzy.entity.Files;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 *
 * FilesService
 *
 * @author 杨子宇
 * @createTime 2023-04-19
 */
public interface IFilesService extends IService<Files> {

}
