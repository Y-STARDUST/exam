package com.yzy.service.impl;

import com.yzy.entity.Exam;
import com.yzy.mapper.ExamMapper;
import com.yzy.service.IExamService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 *
 * ExamServiceImpl
 *
 * @author 杨子宇
 * @createTime 2023-04-21
 */
@Service
public class ExamServiceImpl extends ServiceImpl<ExamMapper, Exam> implements IExamService {
    public List<Exam> findAll() {
        return list();
    }

    public Exam findById(Integer id) {
        return getById(id);
    }

    public boolean insertOrUpdate(Exam exam) {
        return saveOrUpdate(exam);
    }

    public boolean deleteById(Integer id) {
        return removeById(id);
    }

    public boolean deleteExams(List<Integer> ids) {
        return removeByIds(ids);
    }
}
