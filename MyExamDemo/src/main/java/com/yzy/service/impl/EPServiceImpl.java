package com.yzy.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.yzy.entity.EP;
import com.yzy.mapper.EPMapper;
import com.yzy.service.IEPService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 *
 * EPServiceImpl
 *
 * @author 杨子宇
 * @createTime 2023-04-21
 */
@Service
public class EPServiceImpl extends ServiceImpl<EPMapper, EP> implements IEPService {
    public List<EP> findAll() {
        return list();
    }

    public EP findById(Integer id) {
        return getById(id);
    }

    @Transactional
    public void addPaper(EP ep) {
        remove(new UpdateWrapper<EP>().eq("exam_id",ep.getExamId()));
        save(ep);
    }

    public boolean deleteById(Integer id) {
        return removeById(id);
    }

    public boolean deleteEPs(List<Integer> ids) {
        return removeByIds(ids);
    }

    public Page<EP> findPage(Integer pageNum, Integer pageSize) {
        Page<EP> page = new Page<>(pageNum,pageSize);
        QueryWrapper<EP> queryWrapper = new QueryWrapper<>();
        return page(page, queryWrapper);
    }
}
