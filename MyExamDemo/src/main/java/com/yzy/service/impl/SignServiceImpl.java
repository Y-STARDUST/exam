package com.yzy.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.yzy.common.Code;
import com.yzy.entity.Sign;
import com.yzy.exception.ServiceException;
import com.yzy.mapper.SignMapper;
import com.yzy.service.ISignService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

import java.sql.SQLException;
import java.util.List;

/**
 *
 * SignServiceImpl
 *
 * @author 杨子宇
 * @createTime 2023-04-25
 */
@Service
public class SignServiceImpl extends ServiceImpl<SignMapper, Sign> implements ISignService {
    public List<Sign> findAll() {
        return list();
    }

    public Sign findById(Integer id) {
        return getById(id);
    }

    public void insertOrUpdate(Sign sign) {
        try{
           saveOrUpdate(sign);
        }catch (Exception e){
            throw new ServiceException(Code.code_401,"您已经报名过了，请勿重复报名！");
        }
    }

    public boolean deleteById(Integer id) {
        return removeById(id);
    }

    public boolean deleteSigns(List<Integer> ids) {
        return removeByIds(ids);
    }

    public Page<Sign> findPage(Integer pageNum, Integer pageSize) {
        Page<Sign> page = new Page<>(pageNum,pageSize);
        QueryWrapper<Sign> queryWrapper = new QueryWrapper<>();
        return page(page, queryWrapper);
    }
}
