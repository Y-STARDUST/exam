package com.yzy.service.impl;

import cn.hutool.core.bean.BeanUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.yzy.common.Code;
import com.yzy.controller.dto.TeaResetPasswordDTO;
import com.yzy.entity.Teacher;
import com.yzy.exception.ServiceException;
import com.yzy.mapper.TeacherMapper;
import com.yzy.service.ITeacherService;
import com.yzy.utils.TeaTokenUtils;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * TeacherServiceImpl
 *
 * @author 杨子宇
 * @createTime 2023-04-17
 */
@Service
public class TeacherServiceImpl extends ServiceImpl<TeacherMapper, Teacher> implements ITeacherService {

    @Resource
    private TeacherMapper teacherMapper;

    public Teacher teaLogin(Teacher teacher) {
        QueryWrapper<Teacher> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("username", teacher.getUsername());
        queryWrapper.eq("password", teacher.getPassword());
        Teacher one;
        try {
            one = getOne(queryWrapper);
        } catch (Exception e) {
            throw new ServiceException(Code.code_500, "系统错误！");
        }

        if (one != null) {
            BeanUtil.copyProperties(one,teacher,true);

            // 设置教师token
            String token = TeaTokenUtils.getToken(one);
            teacher.setToken(token);

            return teacher;
        } else {
            throw new ServiceException(Code.code_600, "用户名或密码错误！");
        }
    }

    public Teacher teaRegister(Teacher teacher) {
        QueryWrapper<Teacher> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("username", teacher.getUsername());

        Teacher one;
        try {
            one = getOne(queryWrapper);
        } catch (Exception e) {
            throw new ServiceException(Code.code_500, "系统错误！");
        }

        if (one == null) {
            save(teacher);
        } else {
            throw new ServiceException(Code.code_600, "用户已存在！");
        }

        return teacher;
    }

    public Teacher findOne(String username) {
        QueryWrapper<Teacher> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("username",username);
        return getOne(queryWrapper);
    }

    public Teacher upDataInfo(Teacher teacher) {
        QueryWrapper<Teacher> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("tid",teacher.getTid());
        update(teacher,queryWrapper);

        return teacher;
    }

    public void updatePassword(TeaResetPasswordDTO teaResetPasswordDTO) {
        int update = teacherMapper.updatePassword(teaResetPasswordDTO);
        if (update < 1){
            throw new ServiceException(Code.code_401,"原密码错误！");
        }
    }

    public Teacher resetPassword(Teacher teacher) {
        QueryWrapper<Teacher> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("username", teacher.getUsername());
        Teacher one;
        try {
            one = getOne(queryWrapper);
        } catch (Exception e) {
            throw new ServiceException(Code.code_500, "系统错误！");
        }

        if (one == null) {
            throw new ServiceException(Code.code_600, "要找回的用户不存在！");
        } else {
            update(teacher,queryWrapper);
        }

        return teacher;
    }
}
