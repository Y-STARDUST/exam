package com.yzy.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.yzy.entity.PQ;
import com.yzy.entity.Question;
import com.yzy.mapper.PQMapper;
import com.yzy.service.IPQService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 *
 * PQServiceImpl
 *
 * @author 杨子宇
 * @createTime 2023-04-21
 */
@Service
public class PQServiceImpl extends ServiceImpl<PQMapper, PQ> implements IPQService {
    public List<PQ> findAll() {
        return list();
    }

    public PQ findById(Integer id) {
        return getById(id);
    }

    public boolean insertOrUpdate(PQ pq) {
        return saveOrUpdate(pq);
    }

    public boolean deleteById(Integer id) {
        return removeById(id);
    }

    public boolean deletePQs(List<Integer> ids) {
        return removeByIds(ids);
    }

    public Page<PQ> findPage(Integer pageNum, Integer pageSize) {
        Page<PQ> page = new Page<>(pageNum,pageSize);
        QueryWrapper<PQ> queryWrapper = new QueryWrapper<>();
        return page(page, queryWrapper);
    }

    @Resource
    private PQMapper paperQuestionMapper;
    @Override
    public List<Question> selectQuestions(Integer paperId) {
        return paperQuestionMapper.selectQuestions(paperId);
    }
}
