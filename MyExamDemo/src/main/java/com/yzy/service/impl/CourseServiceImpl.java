package com.yzy.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.yzy.entity.Course;
import com.yzy.mapper.CourseMapper;
import com.yzy.service.ICourseService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 *
 * CourseServiceImpl
 *
 * @author 杨子宇
 * @createTime 2023-04-21
 */
@Service
public class CourseServiceImpl extends ServiceImpl<CourseMapper, Course> implements ICourseService {
    public List<Course> findAll() {
        return list();
    }

    public Course findById(Integer id) {
        return getById(id);
    }

    public boolean insertOrUpdate(Course course) {
        return saveOrUpdate(course);
    }

    public boolean deleteById(Integer id) {
        return removeById(id);
    }

    public boolean deleteCourses(List<Integer> ids) {
        return removeByIds(ids);
    }

    public Page<Course> findPage(Integer pageNum, Integer pageSize,String courseName,String remark) {
        Page<Course> page = new Page<>(pageNum,pageSize);
        QueryWrapper<Course> queryWrapper = new QueryWrapper<>();
        if (!"".equals(courseName)) {
            queryWrapper.or().like("name", courseName);
        }
        if (!"".equals(remark)) {
            queryWrapper.or().like("remark", remark);
        }
        return page(page, queryWrapper);
    }
}
