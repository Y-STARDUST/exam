package com.yzy.service.impl;

import cn.hutool.core.date.DateUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.yzy.entity.Question;
import com.yzy.mapper.QuestionMapper;
import com.yzy.service.IQuestionService;
import com.yzy.utils.TeaTokenUtils;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * QuestionServiceImpl
 *
 * @author 杨子宇
 * @createTime 2023-04-20
 */
@Service
public class QuestionServiceImpl extends ServiceImpl<QuestionMapper, Question> implements IQuestionService {
    public List<Question> findAll() {
        return list();
    }

    public Question findById(Integer id) {
        return getById(id);
    }

    public boolean insertOrUpdate(Question question) {
        if (question.getId() == null) {
            question.setTime(DateUtil.now());
            question.setTeacherName(TeaTokenUtils.getCurrentTeacher().getUsername());
        }

        return saveOrUpdate(question);
    }

    public boolean deleteById(Integer id) {
        return removeById(id);
    }

    public boolean deleteQuestions(List<Integer> ids) {
        return removeByIds(ids);
    }

    public Page<Question> findPage(Integer pageNum,
                                   Integer pageSize,
                                   String name,
                                   String a,
                                   String b,
                                   String c,
                                   String d,
                                   String answer,
                                   Integer courseId,
                                   Integer type) {
        Page<Question> page = new Page<>(pageNum, pageSize);
        QueryWrapper<Question> queryWrapper = new QueryWrapper<>();
        if (!"".equals(name)) {
            queryWrapper.or().like("name", name);
        }
        if (!"".equals(a)) {
            queryWrapper.or().like("a", a);
        }
        if (!"".equals(b)) {
            queryWrapper.or().like("b", b);
        }
        if (!"".equals(c)) {
            queryWrapper.or().like("c", c);
        }
        if (!"".equals(d)) {
            queryWrapper.or().like("d", d);
        }
        if (!"".equals(answer)) {
            queryWrapper.or().like("answer", answer);
        }
        if (courseId != null) {
            queryWrapper.eq("course_id",courseId);
        }
        if (type != null) {
            queryWrapper.eq("type",type);
        }
        return page(page, queryWrapper);
    }
}
