package com.yzy.service;

import com.yzy.entity.Student;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 *
 * StudentService
 *
 * @author 杨子宇
 * @createTime 2023-04-18
 */
public interface IStudentService extends IService<Student> {

}
