package com.yzy.service;

import com.yzy.entity.Teacher;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 *
 * TeacherService
 *
 * @author 杨子宇
 * @createTime 2023-04-17
 */
public interface ITeacherService extends IService<Teacher> {

}
