package com.yzy.service;

import com.yzy.entity.EP;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 *
 * EPService
 *
 * @author 杨子宇
 * @createTime 2023-04-21
 */
public interface IEPService extends IService<EP> {

}
