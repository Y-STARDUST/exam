package com.yzy.controller;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.yzy.common.Result;
import org.springframework.web.bind.annotation.*;
import javax.annotation.Resource;
import java.util.List;

import com.yzy.service.impl.SignServiceImpl;
import com.yzy.entity.Sign;

import org.springframework.web.bind.annotation.RestController;

/**
 *
 * SignController
 *
 * @author 杨子宇
 * @createTime  2023-04-25
 */
@RestController
@RequestMapping("/sign")
public class SignController {

    @Resource
    private SignServiceImpl signService;

    /**
    * 查询全部
    */
    @GetMapping
    public List<Sign> findAll(){
        return signService.findAll();
    }

    /**
    * 根据id查询
    */
    @GetMapping("/{id}")
    public Sign findById(@PathVariable Integer id){
        return signService.findById(id);
    }

    /**
    * 分页查询
    */
    @GetMapping("/page")
    public Page<Sign> findPage(@RequestParam Integer pageNum,@RequestParam Integer pageSize){
        return signService.findPage(pageNum,pageSize);
    }

    /**
    * 新增或修改
    */
    @PostMapping
    public Result insertOrUpdate(@RequestBody Sign sign){
        signService.insertOrUpdate(sign);
        return Result.success();
    }

    /**
    * 根据id删除
    */
    @DeleteMapping("/{id}")
    public boolean deleteById(@PathVariable Integer id){
        return signService.deleteById(id);
    }

    /**
    * 批量删除
    */
    @PostMapping("/del/batch")
    private boolean deleteSigns(@RequestBody List<Integer> ids){
        return signService.deleteSigns(ids);
    }
}
