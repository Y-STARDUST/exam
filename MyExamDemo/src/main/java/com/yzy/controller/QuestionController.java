package com.yzy.controller;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.yzy.entity.Question;
import com.yzy.service.impl.QuestionServiceImpl;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

/**
 * QuestionController
 *
 * @author 杨子宇
 * @createTime 2023-04-20
 */
@RestController
@RequestMapping("/question")
public class QuestionController {

    @Resource
    private QuestionServiceImpl questionService;

    /**
     * 查询全部
     */
    @GetMapping
    public List<Question> findAll() {
        return questionService.findAll();
    }

    /**
     * 根据id查询
     */
    @GetMapping("/{id}")
    public Question findById(@PathVariable Integer id) {
        return questionService.findById(id);
    }

    /**
     * 分页查询
     */
    @GetMapping("/page")
    public Page<Question> findPage(@RequestParam Integer pageNum,
                                   @RequestParam Integer pageSize,
                                   @RequestParam(defaultValue = "") String name,
                                   @RequestParam(defaultValue = "") String a,
                                   @RequestParam(defaultValue = "") String b,
                                   @RequestParam(defaultValue = "") String c,
                                   @RequestParam(defaultValue = "") String d,
                                   @RequestParam(defaultValue = "") String answer,
                                   @RequestParam(required = false) Integer courseId,
                                   @RequestParam(required = false) Integer type) {
        return questionService.findPage(pageNum, pageSize,name,a,b,c,d,answer,courseId,type);
    }

    /**
     * 新增或修改
     */
    @PostMapping
    public boolean insertOrUpdate(@RequestBody Question question) {
        return questionService.insertOrUpdate(question);
    }

    /**
     * 根据id删除
     */
    @DeleteMapping("/{id}")
    public boolean deleteById(@PathVariable Integer id) {
        return questionService.deleteById(id);
    }

    /**
     * 批量删除
     */
    @PostMapping("/del/batch")
    private boolean deleteQuestions(@RequestBody List<Integer> ids) {
        return questionService.deleteQuestions(ids);
    }
}
