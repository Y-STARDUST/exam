package com.yzy.controller;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.springframework.web.bind.annotation.*;
import javax.annotation.Resource;
import java.util.List;

import com.yzy.service.impl.CourseServiceImpl;
import com.yzy.entity.Course;

import org.springframework.web.bind.annotation.RestController;

/**
 *
 * CourseController
 *
 * @author 杨子宇
 * @createTime  2023-04-21
 */
@RestController
@RequestMapping("/course")
public class CourseController {

    @Resource
    private CourseServiceImpl courseService;

    /**
    * 查询全部
    */
    @GetMapping
    public List<Course> findAll(){
        return courseService.findAll();
    }

    /**
    * 根据id查询
    */
    @GetMapping("/{id}")
    public Course findById(@PathVariable Integer id){
        return courseService.findById(id);
    }

    /**
    * 分页查询
    */
    @GetMapping("/page")
    public Page<Course> findPage(@RequestParam Integer pageNum,
                                 @RequestParam Integer pageSize,
                                 @RequestParam(defaultValue = "") String courseName,
                                 @RequestParam(defaultValue = "") String remark){
        return courseService.findPage(pageNum,pageSize,courseName,remark);
    }

    /**
    * 新增或修改
    */
    @PostMapping
    public boolean insertOrUpdate(@RequestBody Course course){
        return courseService.insertOrUpdate(course);
    }

    /**
    * 根据id删除
    */
    @DeleteMapping("/{id}")
    public boolean deleteById(@PathVariable Integer id){
        return courseService.deleteById(id);
    }

    /**
    * 批量删除
    */
    @PostMapping("/del/batch")
    private boolean deleteCourses(@RequestBody List<Integer> ids){
        return courseService.deleteCourses(ids);
    }
}
