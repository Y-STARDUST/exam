package com.yzy.controller.dto;

import lombok.Data;

/**
 * @author 杨子宇
 */
@Data
public class TeaResetPasswordDTO {
    private String username;
    private String password;
    private String newPassword;
}
