package com.yzy.controller.dto;

import lombok.Data;

import java.util.List;

/**
 * 手动组卷数据处理
 *
 * @author 杨子宇
 */
@Data
public class HandPaperDTO {
    private List<Integer> handleQuestionIds;
    private Integer paperId;
}
