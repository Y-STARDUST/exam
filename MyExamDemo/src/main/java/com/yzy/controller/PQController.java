package com.yzy.controller;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.springframework.web.bind.annotation.*;
import javax.annotation.Resource;
import java.util.List;

import com.yzy.service.impl.PQServiceImpl;
import com.yzy.entity.PQ;

import org.springframework.web.bind.annotation.RestController;

/**
 *
 * PQController
 *
 * @author 杨子宇
 * @createTime  2023-04-21
 */
@RestController
@RequestMapping("/p-q")
public class PQController {

    @Resource
    private PQServiceImpl pQService;

    /**
    * 查询全部
    */
    @GetMapping
    public List<PQ> findAll(){
        return pQService.findAll();
    }

    /**
    * 根据id查询
    */
    @GetMapping("/{id}")
    public PQ findById(@PathVariable Integer id){
        return pQService.findById(id);
    }

    /**
    * 分页查询
    */
    @GetMapping("/page")
    public Page<PQ> findPage(@RequestParam Integer pageNum,@RequestParam Integer pageSize){
        return pQService.findPage(pageNum,pageSize);
    }

    /**
    * 新增或修改
    */
    @PostMapping
    public boolean insertOrUpdate(@RequestBody PQ pQ){
        return pQService.insertOrUpdate(pQ);
    }

    /**
    * 根据id删除
    */
    @DeleteMapping("/{id}")
    public boolean deleteById(@PathVariable Integer id){
        return pQService.deleteById(id);
    }

    /**
    * 批量删除
    */
    @PostMapping("/del/batch")
    private boolean deletePQs(@RequestBody List<Integer> ids){
        return pQService.deletePQs(ids);
    }
}
