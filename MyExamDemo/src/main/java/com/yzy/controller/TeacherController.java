package com.yzy.controller;

import com.yzy.common.Result;
import com.yzy.controller.dto.TeaResetPasswordDTO;
import org.springframework.web.bind.annotation.*;
import javax.annotation.Resource;


import com.yzy.service.impl.TeacherServiceImpl;
import com.yzy.entity.Teacher;

import org.springframework.web.bind.annotation.RestController;

/**
 *
 * TeacherController
 *
 * @author 杨子宇
 * @createTime  2023-04-17
 */
@RestController
@RequestMapping("/teacher")
public class TeacherController {

    @Resource
    private TeacherServiceImpl teacherService;

    /**
    * 教师登录
    */
    @PostMapping("/tLogin")
    public Result teaLogin(@RequestBody Teacher teacher){
        return Result.success(teacherService.teaLogin(teacher));
    }

    /**
     * 教师注册
     */
    @PostMapping("/tRegister")
    public Result teaRegister(@RequestBody Teacher teacher){
        return Result.success(teacherService.teaRegister(teacher));
    }

    /**
     * 教师信息
     */
    @GetMapping("/teacherName/{username}")
    public Result findOne(@PathVariable String username){
        return Result.success(teacherService.findOne(username));
    }

    /**
     * 编辑教师个人信息
     */
    @PostMapping("/upDataInfo")
    public Result upDataInfo(@RequestBody Teacher teacher){
        return Result.success(teacherService.upDataInfo(teacher));
    }

    /**
     * 教师修改密码
     */
    @PostMapping("/password")
    public Result password(@RequestBody TeaResetPasswordDTO teaResetPasswordDTO){
        teacherService.updatePassword(teaResetPasswordDTO);
        return Result.success();
    }

    /**
     * 教师找回密码
     */
    @PostMapping("/resetPassword")
    public Result resetPassword(@RequestBody Teacher teacher){
        return Result.success(teacherService.resetPassword(teacher));
    }
}
