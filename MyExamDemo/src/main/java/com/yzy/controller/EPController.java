package com.yzy.controller;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.yzy.common.Result;
import org.springframework.web.bind.annotation.*;
import javax.annotation.Resource;
import java.util.List;

import com.yzy.service.impl.EPServiceImpl;
import com.yzy.entity.EP;

import org.springframework.web.bind.annotation.RestController;

/**
 *
 * EPController
 *
 * @author 杨子宇
 * @createTime  2023-04-21
 */
@RestController
@RequestMapping("/e-p")
public class EPController {

    @Resource
    private EPServiceImpl ePService;

    /**
    * 查询全部
    */
    @GetMapping
    public List<EP> findAll(){
        return ePService.findAll();
    }

    /**
    * 根据id查询
    */
    @GetMapping("/{id}")
    public EP findById(@PathVariable Integer id){
        return ePService.findById(id);
    }

    /**
    * 分页查询
    */
    @GetMapping("/page")
    public Page<EP> findPage(@RequestParam Integer pageNum,@RequestParam Integer pageSize){
        return ePService.findPage(pageNum,pageSize);
    }

    @GetMapping("/exam/{examId}")
    public Result findByExam(@PathVariable Integer examId){
        return Result.success(ePService.getOne(new QueryWrapper<EP>().eq("exam_id",examId)));
    }

    /**
    * 新增或修改
    */
    @PostMapping
    public Result addPaper(@RequestBody EP eP){
        ePService.addPaper(eP);
        return Result.success();
    }

    /**
    * 根据id删除
    */
    @DeleteMapping("/{id}")
    public boolean deleteById(@PathVariable Integer id){
        return ePService.deleteById(id);
    }

    /**
    * 批量删除
    */
    @PostMapping("/del/batch")
    private boolean deleteEPs(@RequestBody List<Integer> ids){
        return ePService.deleteEPs(ids);
    }
}
