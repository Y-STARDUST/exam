package com.yzy.controller;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.yzy.common.Result;
import com.yzy.controller.dto.StuResetPasswordDTO;
import org.springframework.web.bind.annotation.*;
import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

import com.yzy.service.impl.StudentServiceImpl;
import com.yzy.entity.Student;

import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

/**
 *
 * StudentController
 *
 * @author 杨子宇
 * @createTime  2023-04-18
 */
@RestController
@RequestMapping("/student")
public class StudentController {

    @Resource
    private StudentServiceImpl studentService;

    /**
    * 查询全部
    */
    @GetMapping
    public List<Student> findAll(){
        return studentService.findAll();
    }

    /**
    * 根据id查询
    */
    @GetMapping("/{id}")
    public Student findById(@PathVariable Integer id){
        return studentService.findById(id);
    }

    /**
    * 分页查询
    */
    @GetMapping("/page")
    public Page<Student> findPage(@RequestParam Integer pageNum,
                               @RequestParam Integer pageSize,
                               @RequestParam(defaultValue = "") String username,
                               @RequestParam(defaultValue = "") String nickname,
                               @RequestParam(defaultValue = "") String email,
                               @RequestParam(defaultValue = "") String phone,
                               @RequestParam(defaultValue = "") String address,
                               @RequestParam(defaultValue = "") String signature) {
        return studentService.findPage(pageNum,pageSize,username,nickname,email,phone,address,signature);
    }

    /**
    * 新增或修改
    */
    @PostMapping
    public boolean insertOrUpdate(@RequestBody Student student){
        return studentService.insertOrUpdate(student);
    }

    /**
    * 根据id删除
    */
    @DeleteMapping("/{id}")
    public boolean deleteById(@PathVariable Integer id){
        return studentService.deleteById(id);
    }

    /**
    * 批量删除
    */
    @PostMapping("/del/batch")
    private boolean deleteStudents(@RequestBody List<Integer> ids){
        return studentService.deleteStudents(ids);
    }

    /**
     * 导入
     */
    @PostMapping("/import")
    public void importFile(MultipartFile file) {
        studentService.importFile(file);
    }

    /**
     * 导出
     */
    @GetMapping("/export")
    public void exportFile(HttpServletResponse response) {
        studentService.exportFile(response);
    }

    /**
     * 学生登录
     */
    @PostMapping("/sLogin")
    public Result stuLogin(@RequestBody Student student){
        return Result.success(studentService.stuLogin(student));
    }

    /**
     * 学生注册
     */
    @PostMapping("/sRegister")
    public Result stuRegister(@RequestBody Student student){
        return Result.success(studentService.stuRegister(student));
    }

    /**
     * 学生信息
     */
    @GetMapping("/studentName/{username}")
    public Result findOne(@PathVariable String username){
        return Result.success(studentService.findOne(username));
    }

    /**
     * 编辑学生个人信息
     */
    @PostMapping("/upDataInfo")
    public Result upDataInfo(@RequestBody Student student){
        return Result.success(studentService.upDataInfo(student));
    }

    /**
     * 学生修改密码
     */
    @PostMapping("/password")
    public Result password(@RequestBody StuResetPasswordDTO stuResetPasswordDTO){
        studentService.updatePassword(stuResetPasswordDTO);
        return Result.success();
    }

    /**
     * 学生找回密码
     */
    @PostMapping("/resetPassword")
    public Result resetPassword(@RequestBody Student student){
        return Result.success(studentService.resetPassword(student));
    }
}
