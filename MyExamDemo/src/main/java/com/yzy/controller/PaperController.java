package com.yzy.controller;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.yzy.common.Result;
import com.yzy.controller.dto.HandPaperDTO;
import org.springframework.web.bind.annotation.*;
import javax.annotation.Resource;
import java.util.List;

import com.yzy.service.impl.PaperServiceImpl;
import com.yzy.entity.Paper;

import org.springframework.web.bind.annotation.RestController;

/**
 *
 * PaperController
 *
 * @author 杨子宇
 * @createTime  2023-04-21
 */
@RestController
@RequestMapping("/paper")
public class PaperController {

    @Resource
    private PaperServiceImpl paperService;

    /**
    * 查询全部
    */
    @GetMapping
    public List<Paper> findAll(){
        return paperService.findAll();
    }

    /**
    * 根据id查询
    */
    @GetMapping("/{id}")
    public Paper findById(@PathVariable Integer id){
        return paperService.findById(id);
    }

    /**
    * 分页查询
    */
    @GetMapping("/page")
    public Page<Paper> findPage(@RequestParam Integer pageNum,
                                @RequestParam Integer pageSize,
                                @RequestParam(defaultValue = "") String paperName){
        return paperService.findPage(pageNum,pageSize,paperName);
    }

    /**
    * 新增或修改
    */
    @PostMapping
    public Result insertOrUpdate(@RequestBody Paper paper){
        return Result.success(paperService.insertOrUpdate(paper));
    }

    /**
    * 根据id删除
    */
    @DeleteMapping("/{id}")
    public boolean deleteById(@PathVariable Integer id){
        return paperService.deleteById(id);
    }

    /**
    * 批量删除
    */
    @PostMapping("/del/batch")
    private boolean deletePapers(@RequestBody List<Integer> ids){
        return paperService.deletePapers(ids);
    }

    /**
     * 手动组卷
     */
    @PostMapping("/handPaper")
    public Result handPaper(@RequestBody HandPaperDTO handPaperDTO){
        return Result.success(paperService.handPaper(handPaperDTO));
    }

    /**
     * 自动组卷
     */
    @PostMapping("/takePaper")
    public Result takePaper(@RequestBody Paper paper){
        return Result.success(paperService.takePaper(paper));
    }


    /**
     * 查看试卷
     */
    @GetMapping("/view/{paperId}")
    public Result paperView(@PathVariable Integer paperId){
        return Result.success(paperService.paperView(paperId));
    }
}
