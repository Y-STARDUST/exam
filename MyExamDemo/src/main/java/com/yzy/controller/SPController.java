package com.yzy.controller;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.yzy.common.Result;
import org.springframework.web.bind.annotation.*;
import javax.annotation.Resource;
import java.util.List;

import com.yzy.service.impl.SPServiceImpl;
import com.yzy.entity.SP;

import org.springframework.web.bind.annotation.RestController;

/**
 *
 * SPController
 *
 * @author 杨子宇
 * @createTime  2023-04-30
 */
@RestController
@RequestMapping("/s-p")
public class SPController {

    @Resource
    private SPServiceImpl sPService;

    /**
    * 查询全部
    */
    @GetMapping
    public List<SP> findAll(){
        return sPService.findAll();
    }

    /**
    * 根据id查询
    */
    @GetMapping("/{id}")
    public SP findById(@PathVariable Integer id){
        return sPService.findById(id);
    }

    /**
    * 分页查询
    */
    @GetMapping("/page")
    public Page<SP> findPage(@RequestParam Integer pageNum,@RequestParam Integer pageSize){
        return sPService.findPage(pageNum,pageSize);
    }

    /**
    * 新增或修改
    */
    @PostMapping
    public Result insertOrUpdate(@RequestBody SP sP){
        sPService.insertOrUpdate(sP);
        return Result.success();
    }

    /**
    * 根据id删除
    */
    @DeleteMapping("/{id}")
    public boolean deleteById(@PathVariable Integer id){
        return sPService.deleteById(id);
    }

    /**
    * 批量删除
    */
    @PostMapping("/del/batch")
    private boolean deleteSPs(@RequestBody List<Integer> ids){
        return sPService.deleteSPs(ids);
    }
}
