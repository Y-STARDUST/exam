package com.yzy.controller;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.yzy.common.Result;
import org.springframework.web.bind.annotation.*;
import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

import com.yzy.service.impl.FilesServiceImpl;
import com.yzy.entity.Files;

import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

/**
 *
 * FilesController
 *
 * @author 杨子宇
 * @createTime  2023-04-19
 */
@RestController
@RequestMapping("/files")
public class FilesController {
    @Resource
    private FilesServiceImpl filesService;

    /**
     * 分页查询
     */
    @GetMapping("/page")
    public Page<Files> findPage(@RequestParam Integer pageNum,
                                  @RequestParam Integer pageSize,
                                  @RequestParam(defaultValue = "") String name,
                                  @RequestParam(defaultValue = "") String type) {
        return filesService.findPage(pageNum,pageSize,name,type);
    }

    /**
     * 根据id删除
     */
    @DeleteMapping("/{id}")
    public boolean deleteById(@PathVariable Integer id){
        return filesService.deleteById(id);
    }

    /**
     * 批量删除
     */
    @PostMapping("/del/batch")
    private Result deleteFiles(@RequestBody List<Integer> ids){
        return filesService.deleteFiles(ids);
    }

    /**
     * 更新
     */
    @PostMapping("/update")
    public boolean upDate(@RequestBody Files files){
        return filesService.upDate(files);
    }

    /**
     * 上传文件
     */
    @PostMapping("/upload")
    public void upload(@RequestParam MultipartFile file) throws IOException {
        filesService.upload(file);
    }

    /**
     * 下载文件
     */
    @GetMapping("/{file}")
    public void download(@PathVariable String file, HttpServletResponse response) throws IOException {
        filesService.download(file,response);
    }
}
