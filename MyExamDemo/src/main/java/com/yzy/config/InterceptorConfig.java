package com.yzy.config;

import com.yzy.config.intercepor.JwtInterceptor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

/**
 * 拦截器配置
 *
 * @author 杨子宇
 */
@Configuration
public class InterceptorConfig implements WebMvcConfigurer {
    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        registry.addInterceptor(jwtInterceptor())
                // 拦截所有请求，通过判断token是否合法决定是否需要登录
                .addPathPatterns("/**")
                .excludePathPatterns(
                        "/teacher/tLogin",
                        "/teacher/tRegister",
                        "/student/sLogin",
                        "/student/sRegister",
                        "/teacher/resetPassword",
                        "/student/resetPassword",
                        "/exam/*",
                        "/*/export",
                        "/*/import",
                        "/files/*");
    }

    @Bean
    public JwtInterceptor jwtInterceptor(){
        return new JwtInterceptor();
    }
}
