package com.yzy.exception;

import com.yzy.common.Code;
import lombok.Getter;

/**
 * @author 杨子宇
 */
@Getter
public class ServiceException extends RuntimeException{
    private Code code;

    public ServiceException(Code code,String msg){
        super(msg);
        this.code = code;
    }
}
