package com.yzy.utils;

import com.baomidou.mybatisplus.generator.FastAutoGenerator;
import com.baomidou.mybatisplus.generator.config.OutputFile;

import java.util.Collections;

/**
 * 代码生成器
 *
 * @author 杨子宇
 * @createTime 2023-4-7
 */
public class CodeGenerator {
    public static void main(String[] args) {
        generate();
    }

    private static void generate() {
        FastAutoGenerator.create("jdbc:mysql:///mydb", "root", "18848588783" )
                .globalConfig(builder -> {
                    builder.author("杨子宇" ) // 设置作者
                            .fileOverride() // 覆盖已生成文件
                            .outputDir("D:\\My_Java_Project\\MyExamDemo\\src\\main\\java\\" ); // 指定输出目录
                })
                .packageConfig(builder -> {
                    builder.parent("com.yzy" ) // 设置父包名
                            .moduleName(null) // 设置父包模块名
                            .pathInfo(Collections.singletonMap(OutputFile.xml, "D:\\My_Java_Project\\MyExamDemo\\src\\main\\resources\\mapper\\" )); // 设置mapperXml生成路径
                })
                .strategyConfig(builder -> {
                    builder.entityBuilder().enableLombok().enableTableFieldAnnotation(); // 开启 lombok 模型，生成实体时生成字段注解
                    builder.mapperBuilder().enableMapperAnnotation().build(); // 开启 Mapper 注解
                    builder.controllerBuilder().enableHyphenStyle()  // 开启驼峰转连字符
                            .enableRestStyle();  // 开启生成@RestController 控制器
                    builder.addInclude("s_p" ) // 设置需要生成的表名
                            .addTablePrefix("", "" ); // 设置过滤表前缀
                })
                .execute();
    }
}
